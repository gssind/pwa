
importScripts('https://www.gstatic.com/firebasejs/4.4.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/4.4.0/firebase-messaging.js');

 var config = {
    apiKey: "AIzaSyBwmzP3pPWYF2BkJHz_5QLBfylp7tRKfKw",
    authDomain: "testapp-a6b05.firebaseapp.com",
    databaseURL: "https://testapp-a6b05.firebaseio.com",
    projectId: "testapp-a6b05",
    storageBucket: "testapp-a6b05.appspot.com",
    messagingSenderId: "521499987799"
  };
  firebase.initializeApp(config);

const messaging = firebase.messaging();

messaging.setBackgroundMessageHandler(function(payload) {
	const title = 'Hello World';
	const options = {
		body: payload.data.body
	};
	return self.registration.showNotification(title, options);
});